from flask import Flask, request, send_from_directory
import os
import json
import time
import hashlib
from flask.helpers import url_for
from werkzeug.utils import secure_filename
import sys
sys.path.insert(0, 'python')
from src.app.tasks import batch_processing

# Create public directory at startup.
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
UPLOAD_FOLDER = os.path.join(BASE_DIR, "public")
if not os.path.exists(UPLOAD_FOLDER):
    os.makedirs(UPLOAD_FOLDER)

ALLOWED_EXTENSIONS = ['.mp4', '.avi']

app = Flask(__name__, static_url_path="")
app.secret_key = os.urandom(42)
app.config['MAX_CONTENT_LENGTH'] = 60 * 1024 * 1024  # max = 60MB
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


def allowed_file(filename):
    return '.' in filename and os.path.splitext(filename)[1] in ALLOWED_EXTENSIONS


@app.route("/", methods=['GET'])
def get_main_html():
    return send_from_directory("./", "index.html")


@app.route("/public/<path:path>", methods=['GET'])
def get_public(path):
    return send_from_directory("public/", path)


@app.route("/upload_video", methods=["POST"])
def upload_video():
    if request.method == 'POST':
        if 'file' not in request.files:
            return json.dumps({
                'message': "No file part"
            }), 403
        file = request.files['file']
        # If the user does not select a file, the browser submits an
        # empty file without a filename.
        if file.filename == '':
            return json.dumps({
                'message': "No file is selected"
            }), 403
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            _, ext = os.path.splitext(filename)
            upload_ts = time.time()
            filename = f"{upload_ts}_{filename}"
            encoded_filename = hashlib.sha384(
                filename.encode('utf-8')).hexdigest()
            encoded_filename_ext = f'{encoded_filename}{ext}'
            file.save(os.path.join(
                app.config['UPLOAD_FOLDER'], encoded_filename_ext))
            # Trigger
            status, code = batch_processing(jobId=encoded_filename_ext)
            if code != 200 or status['status'] != 'started':
                return json.dumps({
                    "message": "Internal Server Error"
                }), 500

            return json.dumps({
                "message": "OK",
                "url": url_for('.get_public', path=encoded_filename),
                'job_id': encoded_filename,
                'recognized_word': url_for('.get_public', path=f'{encoded_filename}.txt')
            }), 200
        else:
            return json.dumps({
                'message': f"{file.filename}'s extension is not allowed"
            }), 403


if __name__ == '__main__':
    app.run(
        host='0.0.0.0',
        port=5000,
        debug=True,
        threaded=True,
        use_reloader=False
    )
