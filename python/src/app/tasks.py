import json
import os
from threading import Thread
from typing import Callable

from src.lip import infer, init_model, load_video
import argparse

__all__ = [
    'NS',
    'MODEL',
    'batch_processing'
]

NS = argparse.Namespace(
    se=True,
    border=False,
    # cuda=torch.cuda.is_available(),
    cuda=False,
    mouth_crop=True,
    weights='model.pt',
    n_class=500
)

MODEL = init_model(
    args=NS
)


def threaded_task(filename: str, callback: Callable = None):
    print(f"Analysing {filename} ...")
    _, lower_faces, mouths = load_video(
        video_pth=filename,
        size=96,
        mouth_size=88
    )

    if NS.mouth_crop:
        result = infer(mouths, model=MODEL)
    else:
        result = infer(lower_faces, model=MODEL)
    result_fn = os.path.basename(filename).split('.')[0]+'.txt'
    with open(result_fn, 'w') as f:
        f.write(str(result[0]))
    f.close()

    if callback is not None:
        callback({
            "job_id": os.path.basename(filename),
            "result": int(result[0])
        })


def batch_processing(jobId: str):
    filename = f"./python/src/app/public/{jobId}"

    if not os.path.exists(filename):
        return json.dumps({
            "status": "failed"
        }), 500

    th = Thread(target=threaded_task, args=(filename, None))
    th.daemon = True
    th.start()

    return {
        "status": "started"
    }, 200
