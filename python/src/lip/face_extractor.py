#!/usr/bin/env python3

import os
from typing import Any
import numpy as np
from torch import Tensor
import torchvision
import torch.nn.functional as F
import face_alignment


__all__ = ['get_face_and_landmarks']


class VidInfo:
    def __init__(self, yt_id, start_time, end_time, face_x, face_y, outdir):
        self.yt_id = yt_id
        self.start_time = float(start_time)
        self.end_time = float(end_time)
        self.out_video_filename = os.path.join(
            outdir, yt_id + '_' + start_time + '_' + end_time + '.mp4')
        self.out_audio_filename = os.path.join(
            outdir, yt_id + '_' + start_time + '_' + end_time + '.wav')

        self.face_point = (float(face_x), float(face_y))


BATCH_SIZE = 16


def get_face(fa, frames, N, H, W, frame_info):
    frame_index = 0
    for i in range(0, N, BATCH_SIZE):
        batch_frames = frames[i: i + BATCH_SIZE].float()

        resized_frames = F.interpolate(
            batch_frames, (640, 640), mode='bilinear')
        _, _, rH, rW = resized_frames.shape

        for faces in fa.face_detector.detect_from_batch(resized_frames):

            possible_faces = list()
            for face in faces:
                x1, y1, x2, y2, _ = face

                x1, x2 = x1 * (W / rW), x2 * (W / rW)
                y1, y2 = y1 * (H / rH), y2 * (H / rH)

                fH = y2 - y1
                fW = x2 - x1

                if fH < 16 or fW < 16:
                    continue

                midx = (x1 + (x2 - x1) / 2.0) / W
                midy = (y1 + (y2 - y1) / 2.0) / H

                distance = np.linalg.norm(
                    np.array([midx, midy]) - np.array([0.5, 0.5]))
                possible_faces.append([[x1, y1, x2, y2], distance])

            try:
                closest_face, _ = sorted(
                    possible_faces, key=lambda f: f[-1])[0]
                frame_info[frame_index] = {'face_coords': closest_face}
            except IndexError:
                frame_info[frame_index] = {'face_coords': [0, 0, W, H]}

            frame_index += 1

    return frame_info


def get_landmarks(fa, frames, frame_info):
    for f_id, frame in frame_info.items():
        face = np.array([frame['face_coords']])
        face[face < 0] = 0
        frame_info[f_id]['face_coords'] = face[0].astype(
            dtype=np.int32).tolist()

        frame_data = frames[int(f_id)].permute(1, 2, 0)

        landmarks = fa.get_landmarks_from_image(frame_data, face)[0]
        frame_info[f_id]['landmarks'] = landmarks.astype(
            dtype=np.int32).tolist()

    return frame_info


def read_video(video_pth: str) -> Tensor:
    frames, _, _ = torchvision.io.read_video(video_pth, pts_unit='sec')
    frames = frames.permute(0, 3, 1, 2)
    return frames


def get_face_and_landmarks(video_pth: str, device: str = 'cpu') -> Any:
    frames = read_video(video_pth=video_pth)
    N, _, H, W = frames.shape
    fa = face_alignment.FaceAlignment(
        face_alignment.LandmarksType._2D, flip_input=False, device=device)
    frame_info = dict()
    frame_info = get_face(fa, frames, N, H, W, frame_info)
    frame_info = get_landmarks(fa, frames, frame_info)
    return frame_info, frames
