from typing import Any, Tuple
from torch import nn
import torch
from .model import VideoModel
from .face_utils import align_and_crop_face
from .face_extractor import get_face_and_landmarks
import cv2
import numpy as np
import argparse


__all__ = ['CenterCrop', 'load_video', 'init_model', 'infer', 'VideoModel']


def CenterCrop(batch_img, size: Tuple = (88, 88)):
    w, h = batch_img.shape[2], batch_img.shape[1]
    th, tw = size
    x1 = int(round((w - tw))/2.)
    y1 = int(round((h - th))/2.)
    img = batch_img[:, y1:y1+th, x1:x1+tw]
    return img


def load_video(video_pth, size: int = 96, mouth_size: int = 88) -> Any:
    frame_info, frames = get_face_and_landmarks(
        video_pth=video_pth, device='cpu')
    faces = []
    lower_faces = []
    for f_id, frame in frame_info.items():
        face_coords = frame['face_coords']
        landmarks = frame['landmarks']
        frame_data = frames[int(f_id)].permute(1, 2, 0)
        cropped = align_and_crop_face(
            frame=frame_data,
            face_coords=face_coords,
            landmarks=landmarks
        ).permute(1, 2, 0).numpy().astype(np.uint8)
        cropped = cv2.resize(cropped, dsize=(size, size))
        cropped = cv2.cvtColor(cropped, cv2.COLOR_BGR2GRAY)
        lower_cropped = cv2.resize(
            cropped[size//2:, ...], dsize=(mouth_size, mouth_size))
        cropped = torch.from_numpy(cropped[np.newaxis, ...])
        lower_cropped = torch.from_numpy(
            lower_cropped[np.newaxis, ...])
        faces.append(cropped)
        lower_faces.append(lower_cropped)
    faces = torch.cat(faces, dim=0) / 255.
    lower_faces = torch.cat(lower_faces, dim=0) / 255.
    mouths = CenterCrop(faces, size=(mouth_size, mouth_size))
    return faces, lower_faces, mouths


def load_missing(model, pretrained_dict):
    model_dict = model.state_dict()
    pretrained_dict = {k: v for k, v in pretrained_dict.items(
    ) if k in model_dict.keys() and v.size() == model_dict[k].size()}
    missed_params = [k for k, v in model_dict.items(
    ) if k not in pretrained_dict.keys()]

    print('loaded params/tot params:{}/{}'.format(len(pretrained_dict), len(model_dict)))
    print('miss matched params:', missed_params)
    model_dict.update(pretrained_dict)
    model.load_state_dict(model_dict)
    return model


def infer(lower_faces, model):
    lower_faces = lower_faces.unsqueeze(1).unsqueeze(0)
    y = model(lower_faces)
    return y.argmax(-1).cpu().numpy()


def init_model(args: argparse.Namespace) -> nn.Module:
    model = VideoModel(args=args)
    if args.cuda:
        model = model.cuda()
    if args.weights is not None:
        print('load weights')
        weight = torch.load(args.weights, map_location=torch.device(
            'cpu' if not args.cuda else 'cuda'))
        load_missing(model, weight.get('video_model'))
    if not args.cuda:
        return model
    return nn.DataParallel(model)
