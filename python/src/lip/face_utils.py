"""
http://datahacker.rs/010-how-to-align-faces-with-opencv-in-python/
"""

import numpy as np
import torch
import cv2
from torch import Tensor


__all__ = ['align_and_crop_face']


def align_face(frame, face_coords, landmarks):
    landmarks = np.array(landmarks)

    roi = {
        'nose': slice(27, 31),
        'nose_point': slice(30, 31),
        'nostril': slice(31, 36),
        'eye1': slice(36, 42),
        'eye2': slice(42, 48)
    }

    def get_roi_mid_point(roi):
        x, y, w, h = cv2.boundingRect(landmarks[roi])
        mid_x = x + w // 2
        mid_y = y + h // 2
        return mid_x, mid_y

    left_eye = get_roi_mid_point(roi['eye1'])
    right_eye = get_roi_mid_point(roi['eye2'])

    left_eye_x, left_eye_y = left_eye
    right_eye_x, right_eye_y = right_eye

    delta_x = right_eye_x - left_eye_x
    delta_y = right_eye_y - left_eye_y

    try:
        angle = np.arctan(delta_y/delta_x)
        angle = (angle * 180) / np.pi
    except ZeroDivisionError:
        angle = 0

    x1, y1, x2, y2 = face_coords
    center_pred = x1 + ((x2 - x1) // 2), y1 + ((y2 - y1) // 2)
    nose_point = landmarks[roi['nose_point']][0]

    if abs(nose_point[0] - center_pred[0]) > 20:
        return None

    img = frame[y1: y2, x1: x2].numpy()

    # Width and height of the image
    h, w = img.shape[:2]
    # Calculating a center point of the image
    # Integer division "//"" ensures that we receive whole numbers
    center = (w // 2, h // 2)
    # Defining a matrix M and calling
    # cv2.getRotationMatrix2D method
    M = cv2.getRotationMatrix2D(center, (angle), 1.0)
    # Applying the rotation to our image using the
    # cv2.warpAffine method
    rotated = cv2.warpAffine(img, M, (w, h))

    return rotated


def align_and_crop_face(frame, face_coords, landmarks) -> Tensor:
    face = align_face(frame, face_coords, landmarks)
    if face is None:
        return face

    return torch.from_numpy(face).permute(2, 0, 1)
