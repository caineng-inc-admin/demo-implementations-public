from gtts import gTTS, gTTSError, tts


__all__ = [
    'Voicer'
]


class Voicer(object):

    def __init__(self) -> None:
        super().__init__()

    def read(self, text: str, filename: str):
        _tts = gTTS(
            text=text,
            tld='com',
            lang='ja',
            slow=False,
            lang_check=False
        )
        try:
            _tts.save(filename)
        except gTTSError as e:
            tts.log.error(e)
