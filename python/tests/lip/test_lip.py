import unittest
from unittest.case import skipIf
import os, sys
sys.path.insert(0, 'python')
import argparse
import torch
from torch import nn
from src.lip import VideoModel, load_video, infer, init_model


class LipTest(unittest.TestCase):
    def setUp(self) -> None:
        return super().setUp()

    def tearDown(self) -> None:
        return super().tearDown()

    @skipIf(not os.path.exists('model.pt'), "model.pt is needed for this test")
    def test_init_model(self):
        args = argparse.Namespace(
            se=True,
            border=False,
            weights='model.pt',
            n_class=500
        )
        model = VideoModel(args=args)
        self.assertIsNotNone(model)
        self.assertTrue(isinstance(model, nn.Module))
        x = torch.randn(1, 29, 1, 88, 88)
        y = model(x)
        self.assertEqual(1, y.shape[0])
        self.assertEqual(500, y.shape[-1])

    @skipIf(not os.path.exists('sample.mp4'), 'sample video must be set to run this test')
    def test_load_video(self):
        video_path = 'sample.mp4'
        faces, lower_faces, mouths = load_video(
            video_pth=video_path,
            size=96,
            mouth_size=88
        )
        self.assertEqual(
            first=[29, 96, 96],
            second=list(faces.shape)
        )
        self.assertEqual(
            first=[29, 88, 88],
            second=list(lower_faces.shape)
        )
        self.assertEqual(
            first=[29, 88, 88],
            second=list(mouths.shape)
        )

    @skipIf(
        not os.path.exists('model.pt') or not os.path.exists('sample.mp4'),
        'both sample video and model file are needed to run this test'
    )
    def test_infer(self):
        video_path = 'sample.mp4'
        _, lower_faces, _ = load_video(
            video_pth=video_path,
            size=96,
            mouth_size=88
        )
        args = argparse.Namespace(
            se=True,
            border=False,
            weights='model.pt',
            n_class=500,
            cuda=False
        )
        model = init_model(args)
        result = infer(lower_faces=lower_faces, model=model)
        self.assertEqual(
            first=[435],
            second=list(result)
        )


if __name__ == '__main__':
    unittest.main(failfast=True)
