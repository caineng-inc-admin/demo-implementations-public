import pytest
from io import BytesIO
import os, sys, json
sys.path.insert(0, 'python')
try:
    from src.app import APP
except Exception as e:
    print(e)
    sys.exit(0)

APP.config['TESTING'] = True


@pytest.fixture
def client():
    with APP.test_client() as client:
        yield client


@pytest.mark.skipif(
    condition=not os.path.exists('sample.mp4'),
    reason='sample video is needed to run this test'
)
def test_submit_video(client):
    mimetype = 'multipart/form-data'
    with open('sample.mp4', 'rb') as f:
        data = BytesIO(f.read())
    f.close()
    endpoint_url = '/upload_video'
    headers = {
        'Content-Type': mimetype,
        'Accept': 'application/json'
    }
    response = client.post(
        endpoint_url,
        data={
            'file': (data, 'lips.mp4')
        },
        buffered=True,
        headers=headers
    )
    data = json.loads(response.data.decode('utf-8'))
    assert response.status == '200 OK'
    assert data['message'] == 'OK'
    assert 'url' in data
    assert 'job_id' in data
    assert 'recognized_word' in data
    