import unittest
import os
import sys
sys.path.insert(0, os.path.abspath('.'))
from src import Voicer


class VoicerTest(unittest.TestCase):
    text1 = 'かしこまりました。田中は現在不在しておりますので、改めてご連絡させて頂きます。'

    def setUp(self) -> None:
        return super().setUp()

    def tearDown(self) -> None:
        return super().tearDown()

    def test_voice(self):
        voicer = Voicer()
        voicer.read(self.text1, 'ja_denwa.mp3')
        self.assertTrue(os.path.exists('ja_denwa.mp3'))


if __name__ == '__main__':
    unittest.main(failfast=True)
