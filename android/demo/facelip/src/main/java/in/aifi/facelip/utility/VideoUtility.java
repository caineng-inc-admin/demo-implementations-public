package in.aifi.facelip.utility;

import android.graphics.Bitmap;


import org.jcodec.api.android.AndroidSequenceEncoder;
import org.jcodec.common.io.FileChannelWrapper;
import org.jcodec.common.io.NIOUtils;
import org.jcodec.common.model.Rational;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class VideoUtility {

    public static void createLipsVideo(List<Bitmap> images) throws IOException {
        FileChannelWrapper out = null;
        File file = new File("/tmp/lips.mp4");
        try {
            out = NIOUtils.writableFileChannel(file.getAbsolutePath());
            AndroidSequenceEncoder encoder = new AndroidSequenceEncoder(out, Rational.R(30, 1));
            for (Bitmap bitmap: images) {
                encoder.encodeImage(bitmap);
            }
            encoder.finish();
        } finally {
            NIOUtils.closeQuietly(out);
        }

    }
}
