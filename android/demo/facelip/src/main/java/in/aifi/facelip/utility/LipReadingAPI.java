package in.aifi.facelip.utility;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface LipReadingAPI {

    public String UPLOAD_URL = "http://localhost:5000/upload_video";
    @Multipart
    @POST("lips")
    Call<String> postLipsVideo(@Part MultipartBody.Part file,
                               @Part("filename")RequestBody name);
}
