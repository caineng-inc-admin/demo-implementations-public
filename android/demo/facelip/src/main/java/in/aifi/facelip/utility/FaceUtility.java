package in.aifi.facelip.utility;

import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.face.Face;
import com.google.mlkit.vision.face.FaceDetection;
import com.google.mlkit.vision.face.FaceDetector;
import com.google.mlkit.vision.face.FaceDetectorOptions;

import java.util.List;

public class FaceUtility {
    private final FaceDetector faceDetector;

    public FaceUtility() {
        FaceDetectorOptions options = new FaceDetectorOptions.Builder()
                .setClassificationMode(FaceDetectorOptions.CLASSIFICATION_MODE_NONE)
                .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_FAST)
                .setLandmarkMode(FaceDetectorOptions.LANDMARK_MODE_ALL)
                .build();

        faceDetector = FaceDetection.getClient(options);
    }

    @WorkerThread
    public void process(@NonNull InputImage image,
                        OnSuccessListener<List<Face>> successListener,
                        OnFailureListener failureListener) {
        Task<List<Face>> result = faceDetector.process(image)
                .addOnSuccessListener(successListener)
                .addOnFailureListener(failureListener);
    }
}
