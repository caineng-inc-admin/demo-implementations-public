package in.aifi.demoapp.listview;

import android.util.Pair;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MessageRepository {
    private List<Pair<String, Date>> messages;

    public MessageRepository() {
        messages = new ArrayList<>();
    }

    public void add(@NonNull String message, @NonNull Date date) {
        messages.add(new Pair<>(message, date));
    }

    public int count() {
        return messages.size();
    }

    public Pair<String, Date> get(int pos) {
        return messages.get(pos);
    }

    public void clear() {
        messages.clear();
    }
}
