package in.aifi.demoapp;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.io.IOException;
import java.util.Vector;

import in.aifi.demoapp.databinding.FragmentDemo04bBinding;
import in.aifi.demoapp.graphics.Demo04Renderer;
import in.aifi.facelip.utility.mtcnn.Box;
import in.aifi.facelip.utility.mtcnn.MTCNN;

public class Demo04bFragment extends Fragment {

    private final static String TAG = "Demo04bFragment";

    private FragmentDemo04bBinding binding;
    private GLSurfaceView glSurfaceView;
    private ImageView avatar;
    private MTCNN mtcnn = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = FragmentDemo04bBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        glSurfaceView = binding.glSurfaceView;

        glSurfaceView.getPreserveEGLContextOnPause();

        try {
            mtcnn = new MTCNN(this.getActivity().getAssets());
        } catch (IOException ie) {
            ie.printStackTrace();
        }

        Box face = this.detectFace();

        if (face != null && face.landmark.length == 5) {
            float vertices[] = new float[3 * face.landmark.length];
            Point centroid = new Point(0, 0);
            for (int i = 0; i < face.landmark.length; i++) {
                Point point = face.landmark[i];
                vertices[3 * i] = point.x / 128.f;
                vertices[3 * i + 1] = point.y / 128.f;
                vertices[3 * i + 2] = 0.f;
                centroid.x += point.x;
                centroid.y += point.y;
            }
            centroid.x /= (face.landmark.length * 128.f);
            centroid.y /= (face.landmark.length * 128.f);
            short indices[] = new short[] {
                    0, 1, 2,
                    0, 2, 4,
                    1, 3, 4,
                    0, 3, 1
            };
            glSurfaceView.setRenderer(new Demo04Renderer(indices, vertices, centroid));
        }

        avatar = binding.avatar;
        Bitmap bitmap = ((MainActivity)this.getActivity()).getCurrentFace();
        avatar.setImageBitmap(bitmap);
    }

    public Box detectFace() {
        Bitmap face = ((MainActivity)this.getActivity()).getCurrentFace();
        if (face == null) return null;
        Vector<Box> faces = mtcnn.detectFaces(face, face.getHeight() / 5);
        if (faces.size() > 1) {
            Log.d(TAG, Resources.getSystem().getString(R.string.more_than_one_face_error_msg));
            Toast.makeText(getContext(), R.string.more_than_one_face_error_msg, Toast.LENGTH_SHORT).show();
        } else if (faces.isEmpty()) {
            Log.d(TAG, "No face detected");
            Toast.makeText(getContext(), R.string.no_face_detected_error_msg, Toast.LENGTH_SHORT).show();
        } else {
            return faces.get(0);
        }
        return null;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}