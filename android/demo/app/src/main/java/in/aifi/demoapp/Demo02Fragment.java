package in.aifi.demoapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Browser;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;

import com.google.common.util.concurrent.ListenableFuture;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import in.aifi.demoapp.camera.Demo02ImageAnalyzer;
import in.aifi.demoapp.databinding.FragmentDemo02Binding;
import in.aifi.facelip.utility.ImageUtility;

public class Demo02Fragment extends Fragment {
    private static final String TAG = "Demo02";
    private FragmentDemo02Binding binding;
    private PreviewView previewView;
    private ImageCapture imageCapture;
    private Camera camera;
    private Demo02ImageAnalyzer imageAnalyzer;
    private ImageView cameraBtn;
    private TextView transcripts;
    List<String> labels = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = FragmentDemo02Binding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Init views
        previewView = binding.previewView;

        // Init transcript listview
        transcripts = binding.transcripts;
        transcripts.setMovementMethod(LinkMovementMethod.getInstance());
        try {
            String labelPath = ImageUtility.assetFilePath(this.getContext(), "label_sorted.txt");
            labels = ImageUtility.readFileByLines(labelPath);
        } catch (IOException e) {
            Log.d(TAG, Arrays.toString(e.getStackTrace()));
        }
        List<String> finalLabels = labels;
        transcripts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!finalLabels.contains(transcripts.getText().toString())) {
                    return;
                }
                Uri uri = Uri.parse("https://www.signasl.org/sign/" + transcripts.getText().toString().toLowerCase());
                Context context = view.getContext();
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.putExtra(Browser.EXTRA_APPLICATION_ID, context.getPackageName());
                context.startActivity(intent);
            }
        });

        // Init camera
        cameraBtn = binding.cameraBtn;

        imageAnalyzer = new Demo02ImageAnalyzer(this.getActivity(), (text) -> {
            cameraBtn.setClickable(true);
            transcripts.setText((String)text);
        });

        cameraBtn.setOnClickListener(view1 -> {
            imageAnalyzer.toogle();
            imageAnalyzer.clearBuffer();
            transcripts.setText(R.string.recording_lbl);
            cameraBtn.setClickable(false);
        });
        ListenableFuture<ProcessCameraProvider> cameraProviderListenableFuture =
                ProcessCameraProvider.getInstance(this.getActivity());
        cameraProviderListenableFuture.addListener(() -> {
            try {
                ProcessCameraProvider cameraProvider = cameraProviderListenableFuture.get();
                cameraProvider.unbindAll();
                Preview preview = new Preview.Builder().build();
                imageCapture = new ImageCapture.Builder()
                        .setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
                        .build();
                CameraSelector cameraSelector = new CameraSelector.Builder()
                        .requireLensFacing(CameraSelector.LENS_FACING_FRONT)
                        .build();
                ImageAnalysis analysisPipeline = new ImageAnalysis.Builder()
                        .build();
                analysisPipeline.setAnalyzer(ContextCompat.getMainExecutor(this.getContext()),
                        imageAnalyzer);
                preview.setSurfaceProvider(
                        previewView.getSurfaceProvider());
                camera = cameraProvider.bindToLifecycle(
                        this.getActivity(),
                        cameraSelector,
                        preview,
                        imageCapture,
                        analysisPipeline);
            } catch (InterruptedException | ExecutionException e) {
                Log.d(TAG, e.getMessage());
            }
        }, ContextCompat.getMainExecutor(this.getActivity()));
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}