package in.aifi.demoapp.config;

import java.util.HashMap;
import java.util.Map;

public class Constant {
    // DEMO 01
    public static String[] VOICES = new String[] {
            "Ivy",
            "Matthew",
            "Joey",
            "Joana"
    };

    public static String END_CONVERSATION_PHRASE = "Thank you, ";

    public static Map<String, String> getVoiceMap() {
        Map<String, String> voiceMap = new HashMap<>();
        voiceMap.put("Matthew", "ScheduleAppointment_dev");
        voiceMap.put("Ivy", "ScheduleAppointmentIvy");
        voiceMap.put("Joana", "ScheduleAppointmenJoana");
        voiceMap.put("Joey", "ScheduleAppointmentJoey");
        return voiceMap;
    }

    // DEMO 02
    // Each sequence will have 29 images
    public static int IMAGE_BUFFER_MAX_SIZE = 29;

    // Demo 03
    public static final String SCENE_URL = "https://9a733c506cb442b2ba3a5cd3a5bf0ed2.us-east-1.sumerian.aws/?arMode=true";
    public static final String IMAGE_FILENAME = "SumerianAnchorImage.png";
    public static final float IMAGE_WIDTH_IN_METERS = (float)0.18;

    // Demo 04
    public enum FACE_ANALYZER_MODE {
        REGISTRATION,
        AUTHENTICATION,
        NONE
    };
}
