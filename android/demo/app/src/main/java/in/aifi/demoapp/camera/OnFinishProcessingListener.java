package in.aifi.demoapp.camera;

import androidx.annotation.NonNull;

public interface OnFinishProcessingListener {
    void onFinish(@NonNull Object message);
}
