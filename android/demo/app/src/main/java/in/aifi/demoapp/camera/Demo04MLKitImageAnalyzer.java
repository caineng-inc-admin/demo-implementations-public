package in.aifi.demoapp.camera;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.media.Image;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.camera.core.ImageProxy;

import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.face.Face;

import in.aifi.demoapp.config.Constant;
import in.aifi.facelip.utility.FaceUtility;
import in.aifi.facelip.utility.ImageUtility;

public class Demo04MLKitImageAnalyzer extends Demo04ImageAnalyzer {
    private final static String TAG = "Demo04MLKitAnalyzer";
    private final FaceUtility faceUtility;

    public Demo04MLKitImageAnalyzer(Context context, OnFinishProcessingListener listener1) {
        super(context, listener1);
        faceUtility = new FaceUtility();
    }

    @Override
    public void analyze(@NonNull ImageProxy imageProxy) {
        @SuppressLint("UnsafeOptInUsageError") Image image = imageProxy.getImage();
        if (image != null) {
            InputImage inputImage = InputImage.fromMediaImage(image,
                    imageProxy.getImageInfo().getRotationDegrees());
            Bitmap bitmap = ImageUtility.convertImageToBitmap(image);
            faceUtility.process(inputImage,
                    faces -> {
                        if (faces.size() > 1) {
                            Log.d(TAG, "More than one face is forbidden!");
                        } else if (faces.isEmpty()) {
                            Log.d(TAG, "No face detected");
                        } else {
                            // SORRY, this app works with one face only! :-)
                            Face face = faces.get(0);
                            Rect rect = face.getBoundingBox();
                            rect.top = Math.max(0, rect.top);
                            rect.bottom = Math.max(0, rect.bottom);
                            rect.right = Math.max(0, rect.right);
                            rect.left = Math.max(0, rect.left);
                            rect.top = Math.min(bitmap.getHeight(), rect.top);
                            rect.bottom = Math.min(bitmap.getHeight(), rect.bottom);
                            rect.right = Math.min(bitmap.getWidth(), rect.right);
                            rect.left = Math.min(bitmap.getWidth(), rect.left);
                            Bitmap faceBitmap = ImageUtility.crop(bitmap, rect);
                            if (mode == Constant.FACE_ANALYZER_MODE.REGISTRATION) {
                                this.register(faceBitmap);
                            } else if (mode == Constant.FACE_ANALYZER_MODE.AUTHENTICATION) {
                                this.authenticate(faceBitmap);
                            }
                        }
                        imageProxy.close();
                    },
                    e -> {
                        e.printStackTrace();
                        Log.e(TAG, String.valueOf(e));
                        imageProxy.close();
                    });
        } else imageProxy.close();
    }
}
