package in.aifi.demoapp.camera;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.Image;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageProxy;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

import in.aifi.demoapp.config.Constant;
import in.aifi.facelip.utility.LipReadingAPI;
import in.aifi.facelip.utility.ImageUtility;
import in.aifi.facelip.utility.VideoUtility;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class Demo02ImageAnalyzer implements ImageAnalysis.Analyzer {
    private static final String TAG ="ImageAnalyzer";
    private final Context mContext;
    private boolean analyzing = false;
    private Queue<Bitmap> imageSequenceBuffer;
    private OnFinishProcessingListener listener;

    public Demo02ImageAnalyzer(Context context, OnFinishProcessingListener listener1) {
        this.mContext = context;
        listener = listener1;
        imageSequenceBuffer = new LinkedList<>();
    }

    public void toogle() {
        analyzing = !analyzing;
    }

    public void clearBuffer() {
        imageSequenceBuffer.clear();
    }

    @Override
    public void analyze(@NonNull ImageProxy image) {
        // only reside limited number of images in memory
        @SuppressLint("UnsafeOptInUsageError") Image img = image.getImage();
        Bitmap bitmap = ImageUtility.convertImageToBitmap(img);
        imageSequenceBuffer.add(bitmap);
        // remove old images in memory (FIFO)
        while (imageSequenceBuffer.size() > Constant.IMAGE_BUFFER_MAX_SIZE) {
            imageSequenceBuffer.remove();
        }
        if (analyzing && imageSequenceBuffer.size() == Constant.IMAGE_BUFFER_MAX_SIZE) {

            postLipsVideo();

            // clear memory
            imageSequenceBuffer.clear();

            toogle();
        }
        image.close();
    }

    public void postLipsVideo() {
        if (imageSequenceBuffer.size() > 0) {
            try {
                VideoUtility.createLipsVideo(new ArrayList<>(imageSequenceBuffer));
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(LipReadingAPI.UPLOAD_URL)
                        .build();

                File file = new File("/tmp/lips.mp4");
                RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
                MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("lips.mp4", file.getName(), requestBody);
                RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), "lips.mp4");

                LipReadingAPI getResponse = retrofit.create(LipReadingAPI.class);
                Call<String> call = getResponse.postLipsVideo(fileToUpload, filename);
                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        listener.onFinish(response.body());
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Log.d(TAG, Arrays.toString(t.getStackTrace()));
                    }
                });
            } catch (IOException e) {
                Log.d(TAG, Arrays.toString(e.getStackTrace()));
            }
        }
    }
}
