package in.aifi.demoapp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.UserStateDetails;
import com.amazonaws.mobileconnectors.lex.interactionkit.Response;
import com.amazonaws.mobileconnectors.lex.interactionkit.config.InteractionConfig;
import com.amazonaws.mobileconnectors.lex.interactionkit.ui.InteractiveVoiceView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import in.aifi.demoapp.config.Constant;
import in.aifi.demoapp.databinding.FragmentDemo01Binding;
import in.aifi.demoapp.listview.ConversationListAdapter;
import in.aifi.demoapp.listview.MessageRepository;

public class Demo01Fragment extends Fragment
        implements InteractiveVoiceView.InteractiveVoiceListener {

    private static final String TAG = "Demo01";
    private FragmentDemo01Binding binding;
    private InteractiveVoiceView voiceView;
    private ListView conversationListView;
    private MessageRepository messageRepository;
    private Spinner spinnerBar;

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentDemo01Binding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        conversationListView = binding.conversationsView.conversations;
        spinnerBar = binding.spinnerBar;
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this.getActivity(), R.layout.support_simple_spinner_dropdown_item, Constant.VOICES);
        spinnerBar.setAdapter(adapter);
        spinnerBar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d(TAG, "" + adapterView.getItemAtPosition(i));
                endConversation();
                voiceView = null;
                initAWSAmplifySDK();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Log.d(TAG, "Nothing selected");
            }
        });
        messageRepository = new MessageRepository();
        initAWSAmplifySDK();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
        voiceView = null;
        conversationListView = null;
        messageRepository.clear();
    }


    private void initAWSAmplifySDK() {
        voiceView = (InteractiveVoiceView) binding.voiceInterface;
        voiceView.setInteractiveVoiceListener(this);
        AWSMobileClient.getInstance().initialize(this.getActivity(), new Callback<UserStateDetails>() {

            @Override
            public void onResult(UserStateDetails result) {
                Log.d(TAG, "onResult: ");
                voiceView.getViewAdapter().setCredentialProvider(AWSMobileClient.getInstance());
                AWSMobileClient.getInstance().getCredentials();

                String identityId = AWSMobileClient.getInstance().getIdentityId();
                String botName = null;
                String botAlias = null;
                String botRegion = null;
                JSONObject lexConfig;
                try {
                    lexConfig = AWSMobileClient.getInstance().getConfiguration().optJsonObject("Lex");
                    lexConfig = lexConfig.getJSONObject(Constant.getVoiceMap().get(spinnerBar.getSelectedItem()));
                    Log.d(TAG, lexConfig.toString());

                    botName = lexConfig.getString("Name");
                    botAlias = lexConfig.getString("Alias");
                    botRegion = lexConfig.getString("Region");
                } catch (JSONException e) {
                    Log.e(TAG, "onResult: Failed to read configuration", e);
                }

                InteractionConfig lexInteractionConfig = new InteractionConfig(
                        botName,
                        botAlias,
                        identityId);
                voiceView.getViewAdapter().setInteractionConfig(lexInteractionConfig);
                voiceView.getViewAdapter().setAwsRegion(botRegion);
            }

            @Override
            public void onError(Exception e) {
                Log.d(TAG, e.getMessage());
            }
        });
    }

    @Override
    public void dialogReadyForFulfillment(Map<String, String> slots, String intent) {
        Log.d(TAG, String.format(
                Locale.US,
                "Dialog ready for fulfillment:\n\tIntent: %s\n\tSlots: %s",
                intent,
                slots.toString()));
    }

    @Override
    public void onResponse(Response response) {
        Log.d(TAG, "Request: " + response.getInputTranscript());
        Log.d(TAG, "Response: " + response.getTextResponse());
        if (this.getActivity() != null) {
            messageRepository.add(response.getInputTranscript(), new Date());
            messageRepository.add(response.getTextResponse(), new Date());
            final ConversationListAdapter conversationListAdapter = new ConversationListAdapter(this.getActivity(), messageRepository);
            conversationListView.setDivider(null);
            conversationListView.setAdapter(conversationListAdapter);
            conversationListView.setSelection(conversationListAdapter.getCount() - 1);
        }

        if (response.getTextResponse() != null &&
                response.getTextResponse().contains(Constant.END_CONVERSATION_PHRASE)) {
            String[] data = response.getTextResponse().substring(Constant.END_CONVERSATION_PHRASE.length()).split(" ");
            if (data.length == 3) {
                String eventType = data[0];
                String eventDate = data[1];
                String eventTime = data[2];
                try {
                    addCalendarEvent(eventType, eventDate, eventTime);
                } catch (ParseException pe) {
                    Log.d(TAG, pe.getMessage());
                }
            }
        }
    }

    @Override
    public void onError(String responseText, Exception e) {
        Log.d(TAG, "Error: " + responseText + e.getMessage());
        endConversation();
    }

    private void endConversation() {
        messageRepository.clear();
        if (this.getActivity() != null) {
            final ConversationListAdapter conversationListAdapter = new ConversationListAdapter(this.getContext(), messageRepository);
            conversationListView.setDivider(null);
            conversationListView.setAdapter(conversationListAdapter);
        }
    }

    public void addCalendarEvent(@NonNull String name, @NonNull String date, @NonNull String time) throws ParseException {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US);
        Date dateInst;
        dateInst = fmt.parse(date + " " + time);
        Intent intent = new Intent(Intent.ACTION_EDIT);
        // Init intent with calendar event
        intent.setType("vnd.android.cursor.item/event");
        intent.putExtra("title", name);
        intent.putExtra("allDay", false);
        intent.putExtra("rule", "FREQ=YEARLY");
        intent.putExtra("beginTime", dateInst.getTime());
        intent.putExtra("endTime", dateInst.getTime() + 60 * 60 * 1000); // 1 hour long
        startActivity(intent);
    }
}