package in.aifi.demoapp;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.google.common.util.concurrent.ListenableFuture;

import java.util.concurrent.ExecutionException;

import in.aifi.demoapp.camera.Demo04ImageAnalyzer;
import in.aifi.demoapp.camera.Demo04MLKitImageAnalyzer;
import in.aifi.demoapp.config.Constant;
import in.aifi.demoapp.databinding.FragmentDemo04aBinding;

public class Demo04aFragment extends Fragment {
    private static final String TAG = "Demo04a";
    private FragmentDemo04aBinding binding;
    private PreviewView previewView;
    private ImageCapture imageCapture;
    private Demo04ImageAnalyzer imageAnalyzer;
    private ImageView cameraBtn;
    private ImageView registerBtn;
    private TextView numFaceRegisteredTxt;
    private TextView guideTxt;
    private Bitmap face = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = FragmentDemo04aBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Init views
        previewView = binding.previewView;

        // Init text views
        guideTxt = binding.guideTxt;
        numFaceRegisteredTxt = binding.numRegisteredTxt;

        // Init camera
        cameraBtn = binding.cameraBtn;
        registerBtn = binding.registerBtn;

        imageAnalyzer = new Demo04MLKitImageAnalyzer(this.getContext(), bitmap -> {
            cameraBtn.setClickable(true);
            registerBtn.setVisibility(View.VISIBLE);

            if (imageAnalyzer.getMode() == Constant.FACE_ANALYZER_MODE.REGISTRATION) {
                face = (Bitmap) bitmap;
                numFaceRegisteredTxt.setText("1");
                ((MainActivity)this.getActivity()).setCurrentFace(face);
            } else if (imageAnalyzer.getMode() == Constant.FACE_ANALYZER_MODE.AUTHENTICATION) {
                boolean pass = (boolean) bitmap;
                if (pass) {
                    NavHostFragment.findNavController(Demo04aFragment.this)
                            .navigate(R.id.action_FifthFragment_to_SixthFragment);
                } else {
                    Toast.makeText(this.getContext(), R.string.face_does_not_match, Toast.LENGTH_SHORT).show();
                }
            }
            imageAnalyzer.setMode(Constant.FACE_ANALYZER_MODE.NONE);
            guideTxt.setVisibility(View.INVISIBLE);
        });
        imageAnalyzer.setMode(Constant.FACE_ANALYZER_MODE.NONE);

        cameraBtn.setOnClickListener(view1 -> {
            cameraBtn.setClickable(false);
            if (imageAnalyzer.getMode() != Constant.FACE_ANALYZER_MODE.REGISTRATION) {
                imageAnalyzer.setMode(Constant.FACE_ANALYZER_MODE.AUTHENTICATION);
            }
        });

        registerBtn.setOnClickListener(v -> {
            registerBtn.setVisibility(View.INVISIBLE);
            guideTxt.setVisibility(View.VISIBLE);
            guideTxt.setText(R.string.guide_register_lbl);
            imageAnalyzer.setMode(Constant.FACE_ANALYZER_MODE.REGISTRATION);
        });

        if (((MainActivity)this.getActivity()).getCurrentFace() != null) {
            face = ((MainActivity)this.getActivity()).getCurrentFace();
            numFaceRegisteredTxt.setText("1");
            imageAnalyzer.setCurrentFace(face);
        }
        ListenableFuture<ProcessCameraProvider> cameraProviderListenableFuture =
                ProcessCameraProvider.getInstance(this.getActivity());
        cameraProviderListenableFuture.addListener(() -> {
            try {
                ProcessCameraProvider cameraProvider = cameraProviderListenableFuture.get();
                cameraProvider.unbindAll();
                Preview preview = new Preview.Builder().build();
                imageCapture = new ImageCapture.Builder()
                        .setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
                        .build();
                CameraSelector cameraSelector = new CameraSelector.Builder()
                        .requireLensFacing(CameraSelector.LENS_FACING_FRONT)
                        .build();
                ImageAnalysis analysisPipeline = new ImageAnalysis.Builder()
                        .build();
                analysisPipeline.setAnalyzer(ContextCompat.getMainExecutor(this.getContext()),
                        imageAnalyzer);
                preview.setSurfaceProvider(
                        previewView.getSurfaceProvider());
                cameraProvider.bindToLifecycle(
                        this.getActivity(),
                        cameraSelector,
                        preview,
                        imageCapture,
                        analysisPipeline);
            } catch (InterruptedException | ExecutionException e) {
                Log.d(TAG, e.getMessage());
            }
        }, ContextCompat.getMainExecutor(this.getActivity()));
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}