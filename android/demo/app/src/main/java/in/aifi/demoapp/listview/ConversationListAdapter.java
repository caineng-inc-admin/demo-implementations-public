package in.aifi.demoapp.listview;

import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Date;

import in.aifi.demoapp.R;

public class ConversationListAdapter extends BaseAdapter {
    private Context context;
    private MessageRepository messageRepository;
    private static LayoutInflater layoutInflater;

    public ConversationListAdapter(Context ctx, MessageRepository messageRepository) {
        context = ctx;
        this.messageRepository = messageRepository;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return messageRepository.count();
    }

    @Override
    public Object getItem(int i) {
        return messageRepository.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Holder holder;
        Pair<String, Date> data = messageRepository.get(i);
        if (view == null) {
            if (i % 2 == 0) {
                view = layoutInflater.inflate(R.layout.message_item_view, null);
            } else {
                view = layoutInflater.inflate(R.layout.message_item_view_response, null);
            }
            holder = new Holder();
            holder.message = view.findViewById(R.id.message_response);
            holder.timestamp = view.findViewById(R.id.timestamp_response);
            view.setTag(holder);
        } else {
            holder = (Holder) view.getTag();
        }

        holder.message.setText(data.first);
        holder.timestamp.setText(DateFormat.getDateTimeInstance().format(data.second));
        return view;
    }

    private static class Holder {
        TextView message;
        TextView timestamp;
    }
}
