package in.aifi.demoapp.camera;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.media.Image;

import androidx.annotation.NonNull;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageProxy;

import java.io.IOException;
import java.util.Vector;

import in.aifi.demoapp.config.Constant;
import in.aifi.facelip.utility.ImageUtility;
import in.aifi.facelip.utility.mobilefacenet.MobileFaceNet;
import in.aifi.facelip.utility.mtcnn.Align;
import in.aifi.facelip.utility.mtcnn.Box;
import in.aifi.facelip.utility.mtcnn.MTCNN;

public class Demo04ImageAnalyzer implements ImageAnalysis.Analyzer {
    private final static String TAG = "Demo04ImageAnalyzer";
    protected Constant.FACE_ANALYZER_MODE mode = Constant.FACE_ANALYZER_MODE.NONE;
    protected Context mContext;
    protected final OnFinishProcessingListener listener;
    private MTCNN mtcnn = null;
    protected MobileFaceNet mobileFaceNet = null;
    protected Bitmap currentFace = null;

    public Demo04ImageAnalyzer(Context context, OnFinishProcessingListener listener1) {
        this.mContext = context;
        listener = listener1;
        try {
            mtcnn = new MTCNN(mContext.getAssets());
            mobileFaceNet = new MobileFaceNet(mContext.getAssets());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setMode(Constant.FACE_ANALYZER_MODE m) {
        mode = m;
    }

    public Constant.FACE_ANALYZER_MODE getMode() {
        return mode;
    }

    public void setCurrentFace(Bitmap currentFace) {
        this.currentFace = currentFace;
    }

    protected void authenticate(Bitmap face) {
        if (currentFace == null || face == null) {
            listener.onFinish(false);
            return;
        }
        // recognize faces
        float score = mobileFaceNet.compare(currentFace, face);
        if (score > MobileFaceNet.THRESHOLD) {
            listener.onFinish(true);
        } else {
            listener.onFinish(false);
        }
    }

    protected void register(Bitmap face) {
        if (face == null) return;
        // save bitmap
        listener.onFinish(face);
        currentFace = face;
    }

    private Bitmap detectFaces(@NonNull Bitmap bitmap) {
        Bitmap face = null;
        Vector<Box> faces = mtcnn.detectFaces(bitmap, bitmap.getWidth() / 5);
        if (faces.size() == 1) {
            Box box = faces.get(0);
            bitmap = Align.face_align(bitmap, box.landmark);
            faces = mtcnn.detectFaces(bitmap, bitmap.getWidth() / 5);
            if (faces.size() == 1) {
                box.toSquareShape();
                box.limitSquare(bitmap.getWidth(), bitmap.getHeight());
                Rect rect = box.transform2Rect();
                face = ImageUtility.crop(bitmap, rect);
            }
        }
        return face;
    }

    @Override
    public void analyze(@NonNull ImageProxy image) {
        @SuppressLint("UnsafeOptInUsageError") Image img = image.getImage();
        Bitmap bitmap = null;
        Bitmap face;
        if (img != null) {
            bitmap = ImageUtility.convertImageToBitmap(img);
        }
        if (mtcnn != null && bitmap != null) {
            face = this.detectFaces(bitmap);
            if (mode == Constant.FACE_ANALYZER_MODE.REGISTRATION) {
                this.register(face);
            } else if (mode == Constant.FACE_ANALYZER_MODE.AUTHENTICATION){
                this.authenticate(face);
            }
        }
        image.close();
    }
}
