FROM python:3.8

ENV MAINTAINER="aifi <tuan.nguyenanh.brse@gmail.com>"

RUN apt-get update
RUN apt-get install -y g++ gcc libpython3-dev libopenblas-dev ffmpeg
COPY python/requirements.txt /src/requirements.txt
COPY model.pt /src/model.pt

WORKDIR /src
RUN pip3 install -r requirements.txt
RUN pip3 install Cython
RUN apt-get install -y pkg-config \
    libavformat-dev libavcodec-dev libavdevice-dev \
    libavutil-dev libswscale-dev libswresample-dev libavfilter-dev
RUN pip3 install git+https://github.com/PyAV-Org/PyAV.git
# Init FAN's weights
RUN python3 -c 'import face_alignment; fa = face_alignment.FaceAlignment(face_alignment.LandmarksType._2D, flip_input=False, device="cpu");'

ENV PYTHONPATH=/src/

COPY python /src/python

EXPOSE 5000

CMD ["python3", "src/app/app.py"]