Demo AIFI
======

[![Build status](https://ci.appveyor.com/api/projects/status/vr7c1xl8bnxkn7ep?svg=true)](https://ci.appveyor.com/project/caineng-inc-admin/demo-implementations)

This mobile app show demo of several artificial intelligence:

![demo-aifi](./assets/overview.png)

### **Interative Voice Dialog**


### Setup backend for basic features
First, to build the backend,

```bash
$ npm install -g @aws-amplify/cli
```

Then go to [`android/demo`](android/demo) and run

```bash
$ amplify init
```

To initialise a backend, run

```bash
$ amplify add interactions
Using service: Lex, provided by: awscloudformation

Welcome to the Amazon Lex chatbot wizard
You will be asked a series of questions to help determine how to best construct your chatbot.

? Provide a friendly resource name that will be used to label this category in the project: lexsample
? Would you like to start with a sample chatbot, import a chatbot, or start from scratch? Start with a sample
? Choose a sample chatbot: ScheduleAppointment
? Please indicate if your use of this bot is subject to the Children's Online Privacy Protection Act (COPPA).
Learn more: https://www.ftc.gov/tips-advice/business-center/guidance/complying-coppa-frequently-asked-questions No
Successfully added resource
```

**And, most importantly, go to [AWS Lex console interface](https://console.aws.amazon.com/lex/home?region=us-east-1#) > choose the botname ScheduleAppointment_dev > click 'Build' > click 'Publish'.**
After this step, you can use chatbot from the demo Android app.

To integrate with Android Calendar app, please go to [AWS Lex console interface](https://console.aws.amazon.com/lex/home?region=us-east-1#) and add a `Response` message to Chatbot:

![response](./assets/amazon-lex-add-response.png)

### Setup backend for voice changing features

We support the voices of `Ivy`, `Matthew`, `Joey` and `Joana`.
You need to create 4 different chatbots in AWS Lex with corresponding actors.
Then you need to customize IAM roles to give access rights to Cognito and Lex.
The names of chatbots should be the same as the content of [`Constant` class](./android/demo/app/src/main/java/in/aifi/demoapp/config/Constant.java).

## Lip-reading transcription

3DCNN based deep learning model is used to decode lip-reading word.


## Sumerian-based Virtual Trainer

Just follow the tutorial in AWS blog: [part 1](https://aws.amazon.com/jp/blogs/media/how-to-create-a-virtual-trainer-with-amazon-sumerian-virtual-reality-and-amazon-machine-learning-part-1/) and [part 2](https://aws.amazon.com/jp/blogs/media/how-to-create-a-virtual-trainer-with-amazon-sumerian-virtual-reality-and-amazon-machine-learning-part-2/), you will get a nice VR app scene in your hand.

It will take about 5-6 minutes.

## Face Recognition 

We use [MobileFaceNet](https://arxiv.org/abs/1804.07573) model with default 5-point face alignment.
It achiveves 99.55% accuracy in a challenge test set.